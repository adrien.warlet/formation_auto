*** Settings ***
Resource	squash_resources.resource
Resource    squash_resources.resource
Library    SeleniumLibrary

*** Variables ***
&{JDD1}    facility=Tokyo    healthcareProgram=radio_program_medicare    HealthcareProgramChoosed=Medicare
&{JDD2}    facility=Hongkong    healthcareProgram=radio_program_medicaid    HealthcareProgramChoosed=Medicaid
&{JDD3}    facility=Seoul    healthcareProgram=radio_program_none    HealthcareProgramChoosed=None


*** Keywords ***
Test Setup
    Open Browser    ${url}    ${browser}
	Maximize Browser Window

Test Teardown
    Sleep    5
	Close Browser

*** Test Cases ***
Connexion à l'application
    [Tags]    run-connexion
	[Setup]	Test Setup

	Given L'utilisateur est sur la page d'accueil
	When L'utilisateur souhaite prendre un rendez-vous
	Then La page de connexion s'affiche
	When L'utilisateur se connecte    John Doe    ThisIsNotAPassword
	Then L'utilisateur est connecté sur la page de rendez-vous

	[Teardown]	Test Teardown


Prendre un rendez-vous Medicaid
    [Tags]    run-medicaid
	[Setup]	Test Setup

	Given Je suis connecté sur la page de rendez vous
	When Je renseigne les informations obligatoires    ${JDD1.facility}
	And Je choisis HealthcareProgram    ${JDD1.healthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HeatlcareProgram est affiché    ${JDD1.HealthcareProgramChoosed}    ${JDD1.facility}
	[Teardown]	Test Teardown


Prendre un rendez-vous Medicare
    [Setup]	Test Setup

	Given Je suis connecté sur la page de rendez vous
	When Je renseigne les informations obligatoires    ${JDD2.facility}
	And Je choisis HealthcareProgram    ${JDD2.healthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HeatlcareProgram est affiché    ${JDD2.HealthcareProgramChoosed}    ${JDD2.facility}

	[Teardown]	Test Teardown


Prendre un rendez-vous None
    [Setup]	Test Setup

	Given Je suis connecté sur la page de rendez vous
	When Je renseigne les informations obligatoires    ${JDD3.facility}
	And Je choisis HealthcareProgram    ${JDD3.healthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HeatlcareProgram est affiché    ${JDD3.HealthcareProgramChoosed}    ${JDD3.facility}
		[Teardown]	Test Teardown
